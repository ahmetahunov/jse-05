package ru.ahmetahunov.tm.command;

import ru.ahmetahunov.tm.context.Bootstrap;

public abstract class AbstractCommand {

    protected Bootstrap bootstrap;

    public AbstractCommand(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public abstract String getName();

    public abstract String getDescription();

    public abstract void execute() throws Exception;

}