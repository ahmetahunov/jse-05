package ru.ahmetahunov.tm.command;

import ru.ahmetahunov.tm.context.Bootstrap;
import ru.ahmetahunov.tm.exception.ItemCollisionException;
import ru.ahmetahunov.tm.helper.ConsoleHelper;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.service.ProjectService;
import ru.ahmetahunov.tm.service.TaskService;
import ru.ahmetahunov.tm.util.DateUtil;
import java.io.IOException;

public class ProjectCreateCommand extends AbstractCommand {

    public ProjectCreateCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "project-create";
    }

    @Override
    public String getDescription() {
        return "Create new project.";
    }

    @Override
    public void execute() throws IOException {
        ProjectService projectService = bootstrap.getServiceLocator().getProjectService();
        System.out.println("[PROJECT CREATE]");
        Project project = createNewProject();
        try {
            projectService.persist(project);
            System.out.println("[OK]");
        } catch (ItemCollisionException e) {
            replaceProject(project);
        }
    }

    private Project createNewProject() throws IOException {
        Project project = new Project();
        System.out.print("Please enter project name: ");
        project.setName(ConsoleHelper.readMessage().trim());
        System.out.println("Please enter description:");
        project.setDescription(ConsoleHelper.readMessage());
        System.out.print("Please insert start date(example: 01.01.2020): ");
        project.setStartDate(DateUtil.parseDate(ConsoleHelper.readMessage()));
        System.out.print("Please enter finish date(example: 01.01.2020): ");
        project.setFinishDate(DateUtil.parseDate(ConsoleHelper.readMessage()));
        return project;
    }

    private void replaceProject(Project project) throws IOException {
        ProjectService projectService = bootstrap.getServiceLocator().getProjectService();
        TaskService taskService = bootstrap.getServiceLocator().getTaskService();
        System.out.println("Project with this name already exists.");
        String answer;
        do {
            System.out.println("Do you want replace it?<y/n>");
            answer = ConsoleHelper.readMessage();
            if ("n".equals(answer)) {
                System.out.println("[CANCELLED]");
                return;
            }
        } while (!("y".equals(answer)));
        Project removedProject = projectService.removeProject(project.getName());
        if (removedProject != null)
            taskService.removeAllProjectTasks(removedProject.getId());
        projectService.merge(project);
        System.out.println("[OK]");
    }

}