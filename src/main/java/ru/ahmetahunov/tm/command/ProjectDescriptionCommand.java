package ru.ahmetahunov.tm.command;

import ru.ahmetahunov.tm.context.Bootstrap;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.helper.ConsoleHelper;
import ru.ahmetahunov.tm.service.ProjectService;
import ru.ahmetahunov.tm.util.InfoUtil;
import java.io.IOException;

public class ProjectDescriptionCommand extends AbstractCommand {

    public ProjectDescriptionCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "project-description";
    }

    @Override
    public String getDescription() {
        return "Show selected project information.";
    }

    @Override
    public void execute() throws IOException {
        ProjectService projectService = bootstrap.getServiceLocator().getProjectService();
        System.out.println("[PROJECT-DESCRIPTION]");
        System.out.print("Please enter project name: ");
        Project project = projectService.findProject(ConsoleHelper.readMessage());
        if (project == null) {
            System.out.println("Selected project does not exist.");
            return;
        }
        System.out.println(InfoUtil.getInfo(project));
    }

}
