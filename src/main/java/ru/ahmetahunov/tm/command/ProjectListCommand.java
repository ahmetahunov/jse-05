package ru.ahmetahunov.tm.command;

import ru.ahmetahunov.tm.context.Bootstrap;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.service.ProjectService;

public class ProjectListCommand extends AbstractCommand {

    public ProjectListCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "project-list";
    }

    @Override
    public String getDescription() {
        return "Show all available projects";
    }

    @Override
    public void execute() {
        ProjectService projectService = bootstrap.getServiceLocator().getProjectService();
        int i = 1;
        System.out.println("[PROJECT LIST]");
        for (Project project : projectService.listAllItems()) {
            System.out.println(String.format("%d. %s", i++, project.getName()));
        }
    }

}