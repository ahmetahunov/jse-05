package ru.ahmetahunov.tm.command;

import ru.ahmetahunov.tm.context.Bootstrap;
import ru.ahmetahunov.tm.helper.ConsoleHelper;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.service.ProjectService;
import ru.ahmetahunov.tm.service.TaskService;
import java.io.IOException;

public class ProjectSelectCommand extends AbstractCommand {

    public ProjectSelectCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "project-select";
    }

    @Override
    public String getDescription() {
        return "Show selected project with tasks.";
    }

    @Override
    public void execute() throws IOException {
        ProjectService projectService = bootstrap.getServiceLocator().getProjectService();
        TaskService taskService = bootstrap.getServiceLocator().getTaskService();
        System.out.println("[PROJECT SELECT]");
        System.out.print("Please enter project name: ");
        String name = ConsoleHelper.readMessage().trim();
        Project project = projectService.findProject(name);
        if (project == null) {
            System.out.println("Selected project does not exist.");
            return;
        }
        System.out.println(project.getName() + ":");
        int i = 1;
        for (Task task : taskService.listAllProjectTasks(project.getId()))
            System.out.println(String.format("  %d. %s", i++, task.getName()));
    }

}
