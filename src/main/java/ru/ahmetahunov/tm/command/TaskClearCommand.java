package ru.ahmetahunov.tm.command;

import ru.ahmetahunov.tm.context.Bootstrap;

public class TaskClearCommand extends AbstractCommand {

    public TaskClearCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "task-clear";
    }

    @Override
    public String getDescription() {
        return "Remove all available tasks.";
    }

    @Override
    public void execute() {
        bootstrap.getServiceLocator().getTaskService().clearRepository();
        System.out.println("[ALL TASKS REMOVED]");
    }

}