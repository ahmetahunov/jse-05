package ru.ahmetahunov.tm.command;

import ru.ahmetahunov.tm.context.Bootstrap;
import ru.ahmetahunov.tm.exception.ItemCollisionException;
import ru.ahmetahunov.tm.helper.ConsoleHelper;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.service.ProjectService;
import ru.ahmetahunov.tm.service.TaskService;
import ru.ahmetahunov.tm.util.DateUtil;
import java.io.IOException;

public class TaskCreateCommand extends AbstractCommand {

    public TaskCreateCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "task-create";
    }

    @Override
    public String getDescription() {
        return "Create new task.";
    }

    @Override
    public void execute() throws IOException {
        TaskService taskService = bootstrap.getServiceLocator().getTaskService();
        System.out.println("[TASK CREATE]");
        System.out.print("Enter project name or press enter to skip: ");
        Project project = getProject();
        Task task = createNewTask(project);
        try {
            taskService.persist(task);
            System.out.println("[OK]");
        } catch (ItemCollisionException e) {
            replaceTask(task);
        }
    }

    private Task createNewTask(Project project) throws IOException {
        Task task = new Task();
        System.out.print("Please enter task name: ");
        task.setName(ConsoleHelper.readMessage().trim());
        System.out.print("Please enter description: ");
        task.setDescription(ConsoleHelper.readMessage());
        System.out.print("Please enter start date(example: 01.01.2020): ");
        task.setStartDate(DateUtil.parseDate(ConsoleHelper.readMessage()));
        System.out.print("Please enter finish date(example: 01.01.2020): ");
        task.setFinishDate(DateUtil.parseDate(ConsoleHelper.readMessage()));
        if (project != null) {
            task.setProjectId(project.getId());
        }
        return task;
    }

    private Project getProject() throws IOException {
        ProjectService projectService = bootstrap.getServiceLocator().getProjectService();
        String name = ConsoleHelper.readMessage().trim();
        Project project = projectService.findProject(name);
        if (project != null) return project;
        System.out.println(name + " is not available.");
        System.out.println("Do you want use another project?<y/n>");
        if ("y".equals(ConsoleHelper.readMessage()))
            return getProject();
        return null;
    }

    private void replaceTask(Task task) throws IOException {
        TaskService taskService = bootstrap.getServiceLocator().getTaskService();
        System.out.println("Task already exists.");
        String answer;
        do {
            System.out.println("Do you want replace it?<y/n>");
            answer = ConsoleHelper.readMessage();
            if ("n".equals(answer)) {
                System.out.println("[CANCELLED]");
                return;
            }
        }while (!("y".equals(answer)));
        taskService.removeTask(task.getName(), task.getProjectId());
        taskService.merge(task);
        System.out.println("[OK]");
    }

}