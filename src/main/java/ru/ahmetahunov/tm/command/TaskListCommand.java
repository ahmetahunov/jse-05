package ru.ahmetahunov.tm.command;

import ru.ahmetahunov.tm.context.Bootstrap;
import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.service.TaskService;

public class TaskListCommand extends AbstractCommand {

    public TaskListCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "task-list";
    }

    @Override
    public String getDescription() {
        return "Shoe all available tasks.";
    }

    @Override
    public void execute() {
        TaskService taskService = bootstrap.getServiceLocator().getTaskService();
        int i = 1;
        System.out.println("[TASK LIST]");
        for (Task task : taskService.listAllItems()) {
            System.out.println(String.format("%d. %s", i++, task.getName()));
        }
    }

}