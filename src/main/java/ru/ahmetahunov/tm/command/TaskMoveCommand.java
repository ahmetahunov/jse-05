package ru.ahmetahunov.tm.command;

import ru.ahmetahunov.tm.context.Bootstrap;
import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.helper.ConsoleHelper;
import ru.ahmetahunov.tm.service.ProjectService;
import ru.ahmetahunov.tm.service.TaskService;
import java.io.IOException;

public class TaskMoveCommand extends AbstractCommand {

    public TaskMoveCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "task-move";
    }

    @Override
    public String getDescription() {
        return "Change project for task.";
    }

    @Override
    public void execute() throws IOException {
        ProjectService projectService = bootstrap.getServiceLocator().getProjectService();
        TaskService taskService = bootstrap.getServiceLocator().getTaskService();
        System.out.println("[TASK-MOVE]");
        System.out.print("Please enter project name: ");
        String projectId = projectService.findProjectId(ConsoleHelper.readMessage());
        System.out.print("Please enter task name: ");
        String taskName = ConsoleHelper.readMessage();
        Task task = taskService.findTask(taskName, projectId);
        if (task == null) {
            System.out.println("Selected task does not exist.");
            return;
        }
        System.out.print("Please enter new project name: ");
        projectId = projectService.findProjectId(ConsoleHelper.readMessage());
        task.setProjectId(projectId);
        System.out.println("[OK]");
    }

}
