package ru.ahmetahunov.tm.command;

import ru.ahmetahunov.tm.context.Bootstrap;
import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.helper.ConsoleHelper;
import ru.ahmetahunov.tm.service.ProjectService;
import ru.ahmetahunov.tm.service.TaskService;
import java.io.IOException;

public class TaskRemoveCommand extends AbstractCommand {

    public TaskRemoveCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "task-remove";
    }

    @Override
    public String getDescription() {
        return "Remove selected task.";
    }

    @Override
    public void execute() throws IOException {
        ProjectService projectService = bootstrap.getServiceLocator().getProjectService();
        TaskService taskService = bootstrap.getServiceLocator().getTaskService();
        System.out.println("[TASK REMOVE]");
        System.out.print("Enter project name or press enter to skip:");
        String projectId = projectService.findProjectId(ConsoleHelper.readMessage().trim());
        System.out.print("Enter task name: ");
        String name = ConsoleHelper.readMessage().trim();
        Task task = taskService.removeTask(name, projectId);
        if (task == null) {
            System.out.println("Selected task does not exist.");
            return;
        }
        System.out.println("[OK]");
    }

}