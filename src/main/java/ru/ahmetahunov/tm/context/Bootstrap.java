package ru.ahmetahunov.tm.context;

import ru.ahmetahunov.tm.command.*;
import ru.ahmetahunov.tm.helper.ConsoleHelper;
import ru.ahmetahunov.tm.service.ServiceLocator;
import java.util.Map;
import java.util.TreeMap;

public class Bootstrap {

    private Map<String, AbstractCommand> commands;

    private ServiceLocator serviceLocator;

    public ServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    public Map<String, AbstractCommand> getCommands() {
        return commands;
    }

    public void init() throws Exception {
        this.commands = new TreeMap<>();
        this.serviceLocator = new ServiceLocator();
        initCommands();
        startCycle();
    }

    private void initCommands() {
        registry(new HelpCommand(this));
        registry(new ProjectClearCommand(this));
        registry(new ProjectCreateCommand(this));
        registry(new ProjectListCommand(this));
        registry(new ProjectRemoveCommand(this));
        registry(new ProjectSelectCommand(this));
        registry(new ProjectDescriptionCommand(this));
        registry(new TaskClearCommand(this));
        registry(new TaskCreateCommand(this));
        registry(new TaskRemoveCommand(this));
        registry(new TaskListCommand(this));
        registry(new TaskDescriptionCommand(this));
        registry(new TaskMoveCommand(this));
        registry(new ExitCommand(this));
    }
    
    private void registry(AbstractCommand command) {
        if (command == null) return;
        commands.put(command.getName(), command);
    }

    private void startCycle() throws Exception {
        while (true) {
            System.out.print("Please enter command: ");
            String operation = ConsoleHelper.readMessage().trim().toLowerCase();
            AbstractCommand command = commands.get(operation);
            if (command == null) {
                System.out.println("Unknown command.");
                continue;
            }
            command.execute();
            if ("exit".equals(operation)) {
                ConsoleHelper.close();
                break;
            }
            System.out.println();
        }
    }

}
