package ru.ahmetahunov.tm.entity;

import java.util.UUID;

public abstract class AbstractEntity {

    protected String id = UUID.randomUUID().toString();

    protected String name = "";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
