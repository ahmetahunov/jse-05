package ru.ahmetahunov.tm.entity;

import java.util.Date;
import java.util.Objects;

public class Project extends AbstractEntity implements IItem {

    private String description = "";

    private Date startDate = new Date(0);

    private Date finishDate = new Date(0);

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(Date finishDate) {
        this.finishDate = finishDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Project project = (Project) o;
        return name.equals(project.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

}
