package ru.ahmetahunov.tm.entity;

import java.util.Date;
import java.util.Objects;

public class Task extends AbstractEntity implements IItem {

    private String description = "";

    private Date startDate = new Date(0);

    private Date finishDate = new Date(0);

    private String projectId = "";

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(Date finishDate) {
        this.finishDate = finishDate;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return projectId.equals(task.getProjectId()) &&
                name.equals(task.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(projectId, name);
    }

}
