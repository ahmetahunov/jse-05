package ru.ahmetahunov.tm.repository;

import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.exception.IdCollisionException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class ProjectRepository implements IRepository<Project> {

    private Map<String, Project> collection = new HashMap<>();

    public Project persist(Project project) throws IdCollisionException {
        if (collection.containsKey(project.getId()))
            throw new IdCollisionException();
        collection.put(project.getId(), project);
        return project;
    }

    public boolean containsValue(Project project) {
        return collection.containsValue(project);
    }

    public Project merge(Project project) {
        collection.put(project.getId(), project);
        return project;
    }

    public void clear() {
        collection.clear();
    }

    public Project remove(String id){
        return collection.remove(id);
    }

    public Collection<Project> findAll() {
        return collection.values();
    }

    public Project findOne(String id) {
        return collection.get(id);
    }

}
