package ru.ahmetahunov.tm.repository;

import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.exception.IdCollisionException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class TaskRepository implements IRepository<Task> {

    private Map<String, Task> collection = new HashMap<>();

    public Task persist(Task task) throws IdCollisionException {
        if (collection.containsKey(task.getId()))
            throw new IdCollisionException();
        collection.put(task.getId(), task);
        return task;
    }

    public Task merge(Task task) {
        collection.put(task.getId(), task);
        return task;
    }

    public void clear() {
        collection.clear();
    }

    public boolean containsValue(Task task) {
        return collection.containsValue(task);
    }

    public Task remove(String id){
        return collection.remove(id);
    }

    public Collection<Task> findAll() {
        return collection.values();
    }

    public Task findOne(String id) {
        return collection.get(id);
    }

}
