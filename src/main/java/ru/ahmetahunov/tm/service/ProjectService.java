package ru.ahmetahunov.tm.service;

import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.repository.ProjectRepository;

public class ProjectService extends AbstractService<Project> {

    public ProjectService(ProjectRepository projectEntityRepository) {
        super(projectEntityRepository);
    }

    public Project findProject(String name) {
        if (name == null || name.isEmpty()) return null;
        for (Project project : repository.findAll()) {
            if (project.getName().equals(name))
                return project;
        }
        return null;
    }

    public String findProjectId(String name) {
        if (name == null || name.isEmpty()) return null;
        Project project = findProject(name);
        if (project == null) return null;
        return project.getId();
    }

    public Project removeProject(String name) {
        if (name == null || name.isEmpty()) return null;
        Project project = findProject(name);
        if (project == null) return null;
        return remove(project);
    }

}
