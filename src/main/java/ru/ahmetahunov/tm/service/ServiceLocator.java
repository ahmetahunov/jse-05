package ru.ahmetahunov.tm.service;

import ru.ahmetahunov.tm.repository.ProjectRepository;
import ru.ahmetahunov.tm.repository.TaskRepository;

public class ServiceLocator {

    private final ProjectService projectService = new ProjectService(new ProjectRepository());

    private final TaskService taskService = new TaskService(new TaskRepository());

    public ProjectService getProjectService() {
        return projectService;
    }

    public TaskService getTaskService() {
        return taskService;
    }

}
