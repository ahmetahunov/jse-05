package ru.ahmetahunov.tm.service;

import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.repository.TaskRepository;
import java.util.ArrayList;
import java.util.List;

public class TaskService extends AbstractService<Task> {

    public TaskService(TaskRepository repository) {
        super(repository);
    }

    public Task findTask(String name, String projectId) {
        if (name == null || name.isEmpty()) return null;
        if (projectId == null) projectId = "";
        for (Task task : repository.findAll()) {
            if (task.getName().equals(name) && task.getProjectId().equals(projectId))
                return task;
        }
        return null;
    }

    public List<Task> listAllProjectTasks(String projectId) {
        List<Task> tasks = new ArrayList<>();
        if (projectId == null || projectId.isEmpty()) return tasks;
        for (Task task : listAllItems()) {
            if (task.getProjectId().equals(projectId))
                tasks.add(task);
        }
        return tasks;
    }

    public Task removeTask(String name, String projectId) {
        if (name == null || name.isEmpty()) return null;
        Task task = findTask(name, projectId);
        if (task == null) return null;
        return remove(task);
    }

    public void removeAllProjectTasks(String projectId) {
        if (projectId == null || projectId.isEmpty()) return;
        listAllItems().removeIf(x->x.getProjectId().equals(projectId));
    }

}
