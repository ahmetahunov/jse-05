package ru.ahmetahunov.tm.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {

    private static  final SimpleDateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy");

    public static Date parseDate(String date) {
        try {
            return dateFormatter.parse(date);
        } catch (ParseException e) {
            return new Date(0);
        }
    }

    public static String formatDate(Date date) {
        return dateFormatter.format(date);
    }

}
