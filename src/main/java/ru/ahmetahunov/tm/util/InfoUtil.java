package ru.ahmetahunov.tm.util;

import ru.ahmetahunov.tm.entity.IItem;

public class InfoUtil {

    public static String getInfo(IItem entity) {
        StringBuilder sb = new StringBuilder();
        sb.append(entity.getName());
        sb.append("\nDescription: ");
        sb.append(entity.getDescription());
        sb.append("\nStart date: ");
        String date = DateUtil.formatDate(entity.getStartDate());
        if (date.equals("01.01.1970"))
            date = "not set";
        sb.append(date);
        sb.append("\nFinish date: ");
        date = DateUtil.formatDate(entity.getFinishDate());
        if (date.equals("01.01.1970"))
            date = "not set";
        sb.append(date);
        return sb.toString();
    }

}
